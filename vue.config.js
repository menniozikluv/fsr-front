const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      "/CarnetContact/*": {  
        target: "http://localhost:8080", 
        secure: false
      }
    }
  }
});
