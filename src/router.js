import { createRouter, createWebHistory } from 'vue-router';
import UserLogin from './components/UserLogin.vue';
import HomePage from './components/HomePage.vue'
import CreateContact from './components/CreateContact.vue'
import GroupPage from './components/GroupPage.vue'

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'UserLogin',
      component: UserLogin
    },
    {
      path: '/home',
      name: 'HomePage',
      component: HomePage
    },
    {
      path: '/create-contact',
      name: 'CreateContact',
      component: CreateContact
    },
    {
      path: '/group-page',
      name: 'GroupPage',
      component: GroupPage
    },
    {
      path: '/mise-a-jour-contact',
      name: 'UpdateContact',
      component: () => import('@/components/UpdateContact.vue')
    }
  ],
});

export default router;
